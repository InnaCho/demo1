module.exports = {
  testEnvironment: 'node',
  reporters: [
    'default',  [
      'jest-html-reporters',
      {
        filename: 'report.html',
        expand: true,
        pageTitle: 'Здесь могла бы быть ваша реклама'
      }
    ]

  ],
  moduleFileExtensions: ['js', 'json'],
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
  },
  testMatch: ['**/specs/*.spec.*'],
  globals: {
    testTimeout: 50000,
  },
  verbose: true,
  setupFilesAfterEnv: ['jest-allure/dist/setup']
};
